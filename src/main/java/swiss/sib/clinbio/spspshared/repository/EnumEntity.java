package swiss.sib.clinbio.spspshared.repository;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public class EnumEntity extends EntityId {

    @Column( unique = true )
    private String name;

    public boolean is( Enum<?> entity ) {
        return name.equals( entity.name() );
    }
}
