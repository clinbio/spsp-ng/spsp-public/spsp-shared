package swiss.sib.clinbio.spspshared.repository;

import jakarta.persistence.*;
import lombok.Getter;

@MappedSuperclass
@Getter
public class EntityId {
    @Id
    @GeneratedValue( strategy = GenerationType.SEQUENCE )
    protected long id;

    @Version
    protected int entityVersion;
}
