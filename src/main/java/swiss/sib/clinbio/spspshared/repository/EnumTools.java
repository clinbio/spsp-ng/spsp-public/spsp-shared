package swiss.sib.clinbio.spspshared.repository;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.JpaRepository;
import swiss.sib.clinbio.spspshared.tools.Tools;

import java.util.List;

@Slf4j
public class EnumTools {

    public static <EnumT extends Enum<?>, EntityT extends EnumEntity, Repository extends JpaRepository<EntityT, Long>>
    void checkCoherency( Class<EnumT> enumT, Class<EntityT> entityT, Repository repository ) throws Exception {

        Enum<?>[] enumList = enumT.getEnumConstants();
        List<EntityT> entityList = repository.findAll();
        if( !entityList.isEmpty() ) {
            if( entityList.size() != enumList.length )
                throw new Exception( entityT.getTypeName() + " not coherent with database" );

            for( EnumEntity e : entityList )
                Tools.castEnum( enumT, e.getName() );
        }
    }

    public static <EnumT extends Enum<?>, EntityT extends EnumEntity, Repository extends JpaRepository<EntityT, Long>>
    void updateEnumDB( Class<EnumT> enumT, Class<EntityT> entityT, Repository repository ) throws Exception {
        List<EntityT> enumList = repository.findAll();
        for( EnumT e : enumT.getEnumConstants() ) {
            boolean found = false;
            for( EntityT purpose : enumList )
                if( purpose.getName().equalsIgnoreCase( e.name() ) ) {
                    found = true;
                    break;
                }

            if( !found ) {
                EntityT entity = entityT.getDeclaredConstructor().newInstance();
                entity.setName( e.name() );
                repository.save( entity );
                log.error( "Create enum: " + entityT.getName() + "." + e.name() );
            }
        }
    }
}
