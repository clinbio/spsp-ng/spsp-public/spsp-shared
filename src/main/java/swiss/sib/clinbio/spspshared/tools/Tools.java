package swiss.sib.clinbio.spspshared.tools;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;
import swiss.sib.clinbio.spspshared.task.GarbageCollector;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.function.Supplier;
import java.util.regex.Pattern;

@Slf4j
public class Tools {
    //\s matches a space, tab, new line, carriage return, form feed or vertical tab
    static private final Pattern patternSpaces = Pattern.compile( "[\\s\\u00A0\\u2007\\u202F]+" );
    static private final Pattern patternAccent = Pattern.compile( "\\p{InCombiningDiacriticalMarks}+" );
    static private final Pattern patternAscii = Pattern.compile( "[^\\p{ASCII}]" );

    //remove leading and trailing white space and replace repeated spaces by one
    public static String removeRedundantSpaces( String text ) {
        return patternSpaces.matcher( text ).replaceAll( " " ).strip();
    }

    public static String removeUselessSpaces( String text ) {
        return removeRedundantSpaces( text ).replaceAll( "([,.;]) ", "$1" );
    }

    public static String removeAccent( String text ) {
        return patternAccent.matcher( Normalizer.normalize( text, Normalizer.Form.NFD ) ).replaceAll( "" );
    }

    public static String capitalize( String name ) {
        return name.substring( 0, 1 ).toUpperCase( Locale.ROOT ) + name.substring( 1 ).toLowerCase( Locale.ROOT );
    }

    static public String removePostfix( String string, String postfix ) {
        int i = string.lastIndexOf( postfix );
        if( i >= 0 && i == string.length() - postfix.length() )
            return string.substring( 0, i );
        return string;
    }

    public static String safeFileName( String name ) {
        return removeRedundantSpaces( removeAccent( name ) ).replaceAll( "[^a-zA-Z0-9-.]+", "_" );
    }

    public static String removeNoAscii( String text ) {
        return patternAscii.matcher( Normalizer.normalize( text, Normalizer.Form.NFD ) ).replaceAll( "" );
    }

    public static String toSnakeCase( String word ) { //convert word from Camel to snake case
        return word.replaceAll( "([a-z])([A-Z]+)", "$1_$2" ).toLowerCase();
    }

    public static String stripQuote( String text ) {
        text = text.strip();
        if( text.isEmpty() )
            return text;
        char c = text.charAt( 0 );
        if( ( c == '"' || ( c == '\'' ) && text.charAt( text.length() - 1 ) == c ) )
            return text.substring( 1, text.length() - 1 ).strip();
        return text;
    }

    public static String ellipse( String text, int max ) {
        return ellipse( text, max, false );
    }

    public static String ellipse( String text, int max, boolean appendTruncateLength ) {
        if( text.length() <= max )
            return text;

        int end = text.lastIndexOf( ' ', max - 1 ); // Chop at last word.
        if( appendTruncateLength )
            return text.substring( 0, end ) + "..." + ( text.length() - 1 - end );
        return text.substring( 0, end ) + "...";
    }

    public static String getFileExtensions( Path path ) {
        return getFileNameExtensions( path.getFileName().toString() );
    }

    public static String getFileNameExtensions( String fileName ) {
        int extensionIndex = fileName.indexOf( '.' );
        if( extensionIndex < 0 )
            return "";
        return fileName.substring( extensionIndex );
    }

    public static String getFileNameLastExtension( String fileName ) {
        int extensionIndex = fileName.lastIndexOf( '.' );
        if( extensionIndex < 0 )
            return "";
        return fileName.substring( extensionIndex );
    }

    public static String[] getFileNameExtensionsList( String fileName ) {
        String[] extensionList = fileName.split( "\\." );
        return Arrays.copyOfRange( extensionList, 1, extensionList.length );
    }

    public static boolean hasExtension( String fileName, String extension ) {
        String[] extensionList = fileName.split( "\\." );
        for( int i = 1; i < extensionList.length; i++ )
            if( extensionList[ i ].equalsIgnoreCase( extension ) )
                return true;
        return false;
    }

    public static String getFileNameWithoutAnyExtensions( Path path ) {
        return getNameWithoutAnyExtensions( path.getFileName().toString() );
    }

    public static String getNameWithoutAnyExtensions( String fileName ) {
        int extensionIndex = fileName.indexOf( '.' );
        if( extensionIndex < 0 )
            return fileName;
        return fileName.substring( 0, extensionIndex );
    }

    public static Date setDate( int year, int month, int day ) {
        return setDate( year, month, day, 0, 0 );
    }

    public static Date setDate( int year, int month, int day, int hour, int minute ) {
        GregorianCalendar date = new GregorianCalendar();
        date.set( year, month - 1, day, hour, minute );
        return date.getTime();
    }

    public static Date todayDate() {
        LocalDate now = LocalDate.now();
        return Tools.setDate( now.getYear(), now.getMonthValue(), now.getDayOfMonth() );
    }

    public static Date toDate( LocalDate date ) {
        return Date.from( date.atStartOfDay( ZoneId.systemDefault() ).toInstant() );
    }

    public static Date toDate( LocalDateTime date ) {
        return Date.from( date.atZone( ZoneId.systemDefault() ).toInstant() );
    }

    public static LocalDate toDate( Date date ) {
        if( date instanceof java.sql.Date )
            return ( ( java.sql.Date ) date ).toLocalDate();
        return date.toInstant().atZone( ZoneId.systemDefault() ).toLocalDate();
    }

    public static LocalDateTime toTime( Date date ) {
        if( date instanceof java.sql.Date )
            return ( ( java.sql.Date ) date ).toLocalDate().atTime( 0, 0 );
        return date.toInstant().atZone( ZoneId.systemDefault() ).toLocalDateTime();
    }

    public static String dateToString( Date date ) {
        return new SimpleDateFormat( "yyyy-MM-dd" ).format( date );
    }

    public static Date stringToDate( String string ) throws ParseException {
        return new SimpleDateFormat( "yyyy-MM-dd" ).parse( string );
    }

    public static String timeToString( Date date ) {
        if( date == null )
            return "-";
        return new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" ).format( date );
    }

    public static String timeStamp() {
        return new SimpleDateFormat( "yyyyMMddHHmmss" ).format( new Date() );
    }

    public static String timeStampWithMillis() {
        return new SimpleDateFormat( "yyyyMMddHHmmssSSS" ).format( new Date() );
    }

    public static String dateToString( LocalDate date ) {
        return date.format( DateTimeFormatter.ofPattern( "yyyy-MM-dd" ) );
    }

    public static String dateToString( LocalDateTime date ) {
        return date.format( DateTimeFormatter.ofPattern( "yyyy-MM-dd" ) );
    }

    public static String timeToString( LocalDateTime date ) {
        return date.format( DateTimeFormatter.ofPattern( "yyyy-MM-dd HH:mm:ss" ) );
    }

    public static Date getDate( LocalDateTime time ) {
        return Date.from( time.atZone( ZoneId.systemDefault() ).toInstant() );
    }

    public static Date getDate( LocalDate time ) {
        return getDate( time.atStartOfDay() );
    }

    public static <T extends Enum<?>> T castEnum( Class<T> enumeration, String name ) {
        String trimmedName = name.strip();
        for( T t : enumeration.getEnumConstants() ) {
            if( t.toString().equalsIgnoreCase( trimmedName ) ) {
                return t;
            }
        }
        throw new IllegalArgumentException( name + " is not a " + enumeration.getTypeName() );
    }

    //ObjectMapper is thread save, but simultaneous usage is impossible
    public static ObjectMapper jsonMapper() {
        return new ObjectMapper()
                .setDefaultPropertyInclusion( JsonInclude.Include.ALWAYS )
                .setSerializationInclusion( JsonInclude.Include.NON_NULL )
                .registerModule( new JavaTimeModule() )
                .disable( SerializationFeature.WRITE_DATES_AS_TIMESTAMPS );
    }

    public static String getJson( Object object ) throws JsonProcessingException {
        return jsonMapper().writeValueAsString( object );
    }

    public static boolean writeJson( Object object, Path path ) {
        try {
            Files.writeString( path, getJson( object ) );
            return true;
        } catch( IOException ex ) {
            log.error( ex.toString() );
            return false;
        }
    }

    static public RestTemplate restTemplate( int connectTimeoutInMillis, int readTimeoutInMillis ) {
        return new RestTemplateBuilder()
                .setConnectTimeout( Duration.ofMillis( connectTimeoutInMillis ) )
                .setReadTimeout( Duration.ofMillis( readTimeoutInMillis ) )
                .build();
    }

    static public RestTemplate restTemplate( int connectTimeoutInMillis ) {
        return new RestTemplateBuilder()
                .setConnectTimeout( Duration.ofMillis( connectTimeoutInMillis ) )
                .build();
    }

    static public RestTemplate restTemplate() {
        return restTemplate( 5000 );
    }

    public static String htmlText( String text ) {
        return "<html><pre>" + text + "</pre></html>";
    }

    public static void sleep( long millis ) {
        try {
            Thread.sleep( millis );
        } catch( InterruptedException ex ) {
            log.error( ex.toString() );
            Thread.currentThread().interrupt();
        }
    }

    public static <T> T benchmark( String name, Supplier<T> supplier ) {
        LocalDateTime start = LocalDateTime.now();
        long startMemory = GarbageCollector.getUsedMemory();
        T result = supplier.get();
        long durationInMs = start.until( LocalDateTime.now(), ChronoUnit.MILLIS );
        long consumedMemory = GarbageCollector.getUsedMemory() - startMemory;
        log.info( name + " " + durationInMs + " ms " + GarbageCollector.toMB( consumedMemory ) + " MB" );
        return result;
    }

    public static <T> T exceptionCatcher( Callable<T> callable, T whenFail ) {
        try {
            T v = callable.call();
            return ( ( v != null ) ? v : whenFail );
        } catch( Exception ex ) {
            return whenFail;
        }
    }

    public static <T> T exceptionCatcher( Callable<T> supplier ) {
        return exceptionCatcher( supplier, null );
    }

    @FunctionalInterface
    public interface Runnable<E extends Exception> {
        void call() throws E;
    }

    public static <E extends Exception> boolean exceptionToError( Runnable<E> callable ) {
        try {
            callable.call();
            return true;
/*        } catch( RuntimeException runtimeException ) {
            throw runtimeException;*/
        } catch( Exception ex ) {
            log.error( ex.toString() );
            return false;
        }
    }

    public static class Wrapper<T> {
        public Wrapper( T data ) {
            this.data = data;
        }

        public T data;
    }

    /* functionalInterface
        Supplier       ()    -> x
        Consumer       x     -> ()
        Callable       ()    -> x throws ex
        Runnable       ()    -> ()
        Function       x     -> y
        BiFunction     x,y   -> z
        Predicate      x     -> boolean
        UnaryOperator  x1    -> x2
        BinaryOperator x1,x2 -> x3
     */
}
