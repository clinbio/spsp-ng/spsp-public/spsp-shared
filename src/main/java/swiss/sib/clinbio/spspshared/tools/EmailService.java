package swiss.sib.clinbio.spspshared.tools;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.StringJoiner;

@Service
@Slf4j
public class EmailService {
    public enum From {
        DEV( "spsp-dev" ),
        SUPPORT( "spsp-support" );

        private final String from;

        From( String from ) {
            this.from = from;
        }

        String get() {
            return from;
        }
    }

    @Autowired
    private JavaMailSender javaMailSender;

    public boolean sendMail( From from, List<String> mailTo, List<String> mailCc, List<String> mailBcc,
                             String mailSubject, String mailContent, UploadTools.StringResource... attachments ) {
        return sendMail( from.get(),
                toList( mailTo ),
                toList( mailCc ),
                toList( mailBcc ),
                mailSubject, mailContent, attachments );
    }

    private String[] toList( List<String> stringList ) {
        return stringList == null ? null : stringList.toArray( new String[ 0 ] );
    }

    public boolean sendMail( From from, String[] mailTo, String[] mailCc, String[] mailBcc,
                             String mailSubject, String mailContent, UploadTools.StringResource... attachments ) {
        return sendMail( from.get(), mailTo, mailCc, mailBcc, mailSubject, mailContent, attachments );
    }

    private boolean sendMail( String sender, String[] mailTo, String[] mailCc, String[] mailBcc,
                              String mailSubject, String mailContent, UploadTools.StringResource... attachments ) {
        try {
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper( message, true );
            helper.setFrom( sender );
            helper.setTo( mailTo );
            if( mailCc != null && mailCc.length > 0 )
                helper.setCc( mailCc );
            if( mailBcc != null && mailBcc.length > 0 )
                helper.setBcc( mailBcc );
            helper.setSubject( mailSubject );
            helper.setText( mailContent );
            for( UploadTools.StringResource attachment : attachments )
                if( attachment.getFilename() != null )
                    helper.addAttachment( attachment.getFilename(), attachment );

            javaMailSender.send( message );
            return true;

        } catch( MailException | MessagingException ex ) {
            log.error( "The mail to " + mailTo[ 0 ] + " has error: " + ex );
            try {
                StringJoiner mailJoiner = new StringJoiner( "\n" )
                        .add( sender ).add( mailTo[ 0 ] )
                        .add( mailSubject ).add( mailContent );

                String errorId = Tools.timeStamp();
                Path tmpFolder = SharedProperties.get().getTmpFolder();
                for( UploadTools.StringResource attachment : attachments ) {
                    String attachmentName = errorId + "_" + attachment.getFilename();
                    mailJoiner.add( "[attachment] " + attachmentName + " " + ( attachment.contentLength() / ( double ) ( 1 << 20 ) ) + " MB" );
                    Path attachmentPath = tmpFolder.resolve( attachmentName );
                    if( attachment.text.isEmpty() ) {
                        try( FileOutputStream stream = new FileOutputStream( attachmentPath.toFile() ) ) {
                            stream.write( attachment.getByteArray() );
                        }
                    } else
                        Files.writeString( attachmentPath, attachment.text );
                }
                Files.writeString( tmpFolder.resolve( errorId + "_error_mail.txt" ), mailJoiner.toString() );

            } catch( IOException ex2 ) {
                log.error( ex2.toString() );
            }
            return false;
        }
    }
}