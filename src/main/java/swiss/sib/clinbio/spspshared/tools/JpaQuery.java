package swiss.sib.clinbio.spspshared.tools;

import jakarta.persistence.EntityGraph;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;

import java.util.List;
import java.util.function.Consumer;

public class JpaQuery<T> {
    protected final EntityManager entityManager;
    protected final Class<T> entityClass;

    protected JpaQuery( Class<T> entityClass, EntityManager entityManager ) {
        this.entityClass = entityClass;
        this.entityManager = entityManager;
    }

    public static <T> List<T> query( TypedQuery<T> query, EntityGraph<T> entityGraph ) {
        if( entityGraph != null )
            query.setHint( "jakarta.persistence.fetchgraph", entityGraph );
        return query.getResultList();
    }

    public TypedQuery<T> findAll() {
        return entityManager.createQuery( select(), entityClass );
    }

    public TypedQuery<T> findLast( int number ) {
        return entityManager.createQuery(
                        select() + " order by " + entityAlias() + ".id desc", entityClass )
                .setMaxResults( number );
    }

    public TypedQuery<T> findById( List<Long> idList ) {
        return entityManager.createQuery(
                        select() + " where " + entityAlias() + ".id in :IDS", entityClass )
                .setParameter( "IDS", idList );
    }

    public T findById( long id ) {
        return entityManager.createQuery(
                        select() + " where " + entityAlias() + ".id = :ID", entityClass )
                .setParameter( "ID", id )
                .getSingleResult();
    }

    public void delete( T entity ) {
        entityManager.remove( entity );
    }

    public void save( T entity ) {
        entityManager.persist( entity );
    }

    private String select() {
        return "select " + entityAlias() + from();
    }

    private String from() {
        return " from " + entityClass.getSimpleName() + " " + entityAlias();
    }

    private String entityAlias() {
        return "_" + entityClass.getSimpleName().toLowerCase();
    }

    protected <T> String select( Class<T> selectType ) {
        if( selectType.getSimpleName().equals( "Long" ) )
            return "select " + entityAlias() + ".id" + from();
        return select();
    }

    protected <T> String dtoSelect( Class<T> selectType, String select ) {
        return "select new " + selectType.getCanonicalName() + "( " + select + ")" + from();
    }

    public EntityGraph<T> buildEntityGraph( Consumer<EntityGraph<T>> builder ) {
        EntityGraph<T> graph = entityManager.createEntityGraph( entityClass );
        builder.accept( graph );
        return graph;
    }
}
