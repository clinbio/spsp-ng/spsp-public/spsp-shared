package swiss.sib.clinbio.spspshared.tools;

import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.bouncycastle.util.io.Streams;
import org.pgpainless.PGPainless;
import org.pgpainless.algorithm.CompressionAlgorithm;
import org.pgpainless.algorithm.DocumentSignatureType;
import org.pgpainless.decryption_verification.ConsumerOptions;
import org.pgpainless.decryption_verification.DecryptionStream;
import org.pgpainless.decryption_verification.OpenPgpMetadata;
import org.pgpainless.encryption_signing.EncryptionOptions;
import org.pgpainless.encryption_signing.EncryptionStream;
import org.pgpainless.encryption_signing.ProducerOptions;
import org.pgpainless.encryption_signing.SigningOptions;
import org.pgpainless.key.protection.SecretKeyRingProtector;
import org.pgpainless.util.Passphrase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Locale;

//https://github.com/pgpainless/pgpainless
@Service
@Slf4j
public class PgpTool {
    @Value( "${sftp.gpgPassphrase}" )
    private String gpgPassphrase;

    private final Path pgpPrivateKeyPath;

    @Autowired
    PgpTool( SharedProperties properties ) {
        pgpPrivateKeyPath = properties.propertiesFolder.resolve( "pgp_private_key.txt" );
    }

    public String getPublicKey() throws IOException {
        return PGPainless.asciiArmor( PGPainless.extractCertificate( getSecretKey() ) );
    }

    public Path encryptFile( Path inputFile, String recipientPublicKey ) throws IOException, PGPException {
        Path outPath = inputFile.getParent().resolve( inputFile.getFileName().toString() + ".pgp" );
        encryptFile( inputFile, recipientPublicKey, outPath );
        return outPath;
    }

    public void encryptFile(
            Path inputFile,
            String recipientPublicKey,
            Path outputPath
    ) throws IOException, PGPException {
        try( OutputStream outStream = new BufferedOutputStream( new FileOutputStream( outputPath.toFile() ) ) ) {
            EncryptionStream encryptionStream = PGPainless.encryptAndOrSign()
                    .onOutputStream( outStream )
                    .withOptions( setEncodeOptions( recipientPublicKey ) );

            Streams.pipeAll( new FileInputStream( inputFile.toFile() ), encryptionStream );
            encryptionStream.close();
        } catch( PGPException | IOException ex ) {
            if( outputPath.toFile().isFile() )
                Files.delete( outputPath );
            throw ex;
        }
    }

    public Path decryptFile( Path inputFilePath ) throws IOException, PGPException {
        return decryptFile( inputFilePath, null );
    }

    public Path decryptFile( Path inputFilePath, String senderPublicKey ) throws IOException, PGPException {
        String name = inputFilePath.getFileName().toString();
        String lowerCaseName = name.toLowerCase( Locale.ROOT );
        int iExtension = lowerCaseName.indexOf( ".gpg" );
        if( iExtension < 0 )
            iExtension = lowerCaseName.indexOf( ".pgp" );
        if( iExtension < 0 )
            throw new PGPException( "File " + inputFilePath + " has not the right extension" );
        Path outPath = inputFilePath.getParent().resolve( name.substring( 0, iExtension ) );
        decryptFile( inputFilePath, outPath, senderPublicKey );
        return outPath;
    }

    public void decryptFile(
            Path inputFilePath,
            Path outputPath,
            String senderPublicKey ) throws IOException, PGPException {

        try( FileInputStream encryptedInputStream = new FileInputStream( inputFilePath.toFile() );
             OutputStream outStream = new BufferedOutputStream( new FileOutputStream( outputPath.toFile() ) ) ) {

            DecryptionStream decryptionStream = PGPainless.decryptAndOrVerify()
                    .onInputStream( encryptedInputStream )
                    .withOptions( setDecodeOptions( senderPublicKey )
                    );
            Streams.pipeAll( decryptionStream, outStream );
            decryptionStream.close();

            if( senderPublicKey != null ) {
                OpenPgpMetadata metadata = decryptionStream.getResult();
                if( !metadata.isVerified() )
                    throw new PGPException( "Signature not verified" );
            }
        } catch( PGPException | IOException ex ) {
            if( outputPath.toFile().isFile() )
                Files.delete( outputPath );
            throw ex;
        }
    }

    ProducerOptions setEncodeOptions( String recipientPublicKey ) throws IOException, PGPException {
        return ProducerOptions.signAndEncrypt(
                        new EncryptionOptions().addRecipient(
                                PGPainless.readKeyRing().publicKeyRing( recipientPublicKey ) ),
                        new SigningOptions().addInlineSignature(
                                getSecretKeyProtector(), getSecretKey(), DocumentSignatureType.BINARY_DOCUMENT )
                )
                .overrideCompressionAlgorithm( CompressionAlgorithm.UNCOMPRESSED )
                .setAsciiArmor( false );
    }

    ConsumerOptions setDecodeOptions( String senderPublicKey ) throws IOException {
        ConsumerOptions options = new ConsumerOptions();
        options.addDecryptionKey( getSecretKey(), getSecretKeyProtector() );
        if( senderPublicKey != null )
            options.addVerificationCert( PGPainless.readKeyRing().publicKeyRing( senderPublicKey ) );

        return options;
    }

    PGPSecretKeyRing getSecretKey() throws IOException {
        return PGPainless.readKeyRing().secretKeyRing( new FileInputStream( pgpPrivateKeyPath.toFile() ) );
    }

    SecretKeyRingProtector getSecretKeyProtector() {
        return SecretKeyRingProtector.unlockAnyKeyWith( Passphrase.fromPassword( gpgPassphrase ) );
    }
}
