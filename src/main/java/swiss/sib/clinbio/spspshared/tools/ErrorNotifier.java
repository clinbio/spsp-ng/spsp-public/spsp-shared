package swiss.sib.clinbio.spspshared.tools;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;

import java.time.LocalDateTime;
import java.util.List;
import java.util.StringJoiner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class ErrorNotifier extends AppenderBase<ILoggingEvent> {
    private static final long MIN_TIME_BETWEEN_EMAIL_IN_SECONDS = 30;
    private StringJoiner report = new StringJoiner( "\n" );
    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture<?> scheduledTask = null;

    @Override
    protected void append( ILoggingEvent iLoggingEvent ) {
        if( iLoggingEvent.getLevel() != Level.ERROR )
            return;
        report.add( Tools.timeToString( LocalDateTime.now() ) + " " +
                iLoggingEvent.getFormattedMessage() + " from " + iLoggingEvent.getLoggerName() );

        if( scheduledTask != null )
            scheduledTask.cancel( false );
        scheduledTask = scheduler.schedule( this::run, MIN_TIME_BETWEEN_EMAIL_IN_SECONDS, TimeUnit.SECONDS );
    }

    public void run() {
        List<String> monitoringEmails = SharedProperties.get().mailMonitoring;
        if( monitoringEmails == null || monitoringEmails.isEmpty() ) {
            System.out.println( report );
        } else {
            if( !SpringContext.getBean( EmailService.class ).sendMail( EmailService.From.DEV,
                    monitoringEmails, null, null,
                    SharedProperties.get().getApplicationName() + " error",
                    report.toString() ) )
                return;
        }
        report = new StringJoiner( "\n" );
    }
}