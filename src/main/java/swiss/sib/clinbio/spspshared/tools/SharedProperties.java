package swiss.sib.clinbio.spspshared.tools;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

import java.io.IOException;
import java.nio.file.FileStore;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

@Component
@Slf4j
public class SharedProperties {
    public static SharedProperties get() {
        return properties;
    }

    static private SharedProperties properties;

    private static final Date startTime = new Date();
    public final Path runningFolder = Paths.get( "" ).toAbsolutePath();

    public final Path propertiesFolder = Paths.get( "Properties" );

    @Value( "${spsp.mailSupport}" )
    public List<String> mailSupport;

    @Value( "${spsp.mailMonitoring}" )
    public List<String> mailMonitoring;

    @Value( "${spsp.workingFolder}" )
    public Path workingFolder;

    @Autowired
    public SharedProperties( Environment environment ) {
        properties = this;
    }

    public void assertValidity() {
        if( !workingFolder.toFile().isDirectory() )
            throw new RuntimeException( "Property 'workingFolder' not found: " + workingFolder );
        if( mailMonitoring == null )
            throw new RuntimeException( "Property 'mailMonitoring' not found" );
        if( mailSupport == null )
            throw new RuntimeException( "Property 'mailSupport' not found" );
    }

    public static boolean isRelease( Environment environment ) {
        String[] environments = environment.getActiveProfiles();
        if( environments.length != 1 )
            throw new RuntimeException( "BAD ENVIRONMENTS: several profiles" );
        return switch( environments[ 0 ] ) {
            case "release" -> true;
            case "test" -> false;
            default -> throw new RuntimeException( "BAD ENVIRONMENTS: not {'release', 'test'}" );
        };
    }

    public static String getStartTime() {
        return Tools.timeToString( startTime );
    }

    public String getApplicationName() {
        String folderName = getRunningFolderName();
        String name = "UNKNOWN";
        if( folderName.equals( "worker_1_server" ) )
            return "server [WORKER_1]";
        else if( folderName.equals( "worker_2_server" ) )
            return "server [WORKER_2]";
        else if( folderName.equals( "worker_3_loader" ) )
            name = "loader [WORKER_3]";
        else if( folderName.equals( "server" ) )
            name = "server";
        else if( folderName.equals( "loader" ) )
            name = "loader";
        else if( folderName.equals( "portal" ) )
            name = "portal";
        else if( folderName.contains( "server" ) )
            name = "server [DEV]";
        else if( folderName.contains( "loader" ) )
            name = "loader [DEV]";
        else if( folderName.contains( "portal" ) )
            name = "portal [DEV]";
        return "Spsp " + name;
    }

    public String getRunningFolderName() {
        return runningFolder.getFileName().toString();
    }

    public Path getTmpFolder() {
        return workingFolder.resolve( "temp" );
    }

    public Path getExchangeFolder() {
        return workingFolder.resolve( "SPSP-exchange" );
    }

    public static boolean checkRunning( String url ) {
        try {
            String result = Tools.restTemplate( 1000, 1000 ).getForEntity( url, String.class ).getBody();
            return result != null && result.startsWith( "ok" );
        } catch( RestClientException ex ) {
            log.error( "check running " + url + ": " + ex );
            return false;
        }
    }

    public String getWorkDiskUsage() {
        return SharedProperties.getDiskUsage( workingFolder );
    }

    public String getVmDiskUsage() {
        return SharedProperties.getDiskUsage( propertiesFolder );
    }

    public static String getDiskUsage( Path path ) {
        try {
            FileStore store = Files.getFileStore( path );
            long totalSpace = store.getTotalSpace();
            long freeSpace = store.getUsableSpace();
            long usedSpace = totalSpace - freeSpace;

            return store.name() + " " + ( usedSpace >> 30 ) + " GB (" + Math.round( usedSpace / ( double ) totalSpace * 100 ) + "%) used";
        } catch( IOException ex ) {
            return path.getRoot() + " UNAVAILABLE";
        }
    }
}
