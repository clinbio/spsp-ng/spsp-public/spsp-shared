package swiss.sib.clinbio.spspshared.tools;

import org.springframework.core.io.ByteArrayResource;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.zip.GZIPOutputStream;

public class UploadTools {

    static final String BASE64_VALUES = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+/";

    public static String randomBase64( int length ) {
        SecureRandom random = new SecureRandom();
        StringBuilder sb = new StringBuilder( length );
        for( int i = 0; i < length; i++ )
            sb.append( BASE64_VALUES.charAt( random.nextInt( BASE64_VALUES.length() ) ) );
        return sb.toString();
    }

    public static String sha512Hexdigest( String textToHash ) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance( "SHA-512" );
        } catch( NoSuchAlgorithmException ex ) {
            return null;
        }

        byte[] bytes = md.digest( textToHash.getBytes( StandardCharsets.UTF_8 ) );
        return toHex( bytes );
    }

    public static class Digest {
        public static MessageDigest md5() {
            try {
                return MessageDigest.getInstance( "MD5" );
            } catch( NoSuchAlgorithmException ex ) {
                throw new RuntimeException( ex.toString() );
            }
        }

        public static MessageDigest sha256() {
            try {
                return MessageDigest.getInstance( "SHA-256" );
            } catch( NoSuchAlgorithmException ex ) {
                throw new RuntimeException( ex.toString() );
            }
        }

        public static void add( MessageDigest md, String text ) {
            md.update( text.getBytes( StandardCharsets.UTF_8 ) );
        }

        public static String base64Digest( MessageDigest md ) {
            return Base64.getEncoder().encodeToString( md.digest() );
        }

        public static MessageDigest md5Digest( Path filepath ) throws IOException {
            return digest( filepath, md5() );
        }

        public static String md5Base64Digest( Path filepath ) throws IOException {
            return base64Digest( md5Digest( filepath ) );
        }

        public static String sha256Base64Digest( Path filepath ) throws IOException {
            return base64Digest( digest( filepath, sha256() ) );
        }

        public static String sha256HexDigest( Path filepath ) throws IOException {
            return toHex( digest( filepath, sha256() ).digest() );
        }

        public static MessageDigest digest( Path filepath, MessageDigest md ) throws IOException {
            byte[] buf = new byte[ 4096 ];
            try( BufferedInputStream is = new BufferedInputStream( new FileInputStream( filepath.toFile() ) ) ) {
                int n;
                while( ( n = is.read( buf ) ) > 0 )
                    md.update( buf, 0, n );
            }
            return md;
        }
    }

    private static final char[] hexCode = "0123456789abcdef".toCharArray();

    public static String toHex( byte[] data ) {
        StringBuilder sb = new StringBuilder( data.length * 2 );
        for( byte b : data )
            sb.append( hexCode[ ( b >> 4 ) & 0x0f ] ).append( hexCode[ ( b & 0x0f ) ] );

        return sb.toString();
    }

    public static String base64toHex( String base64String ) {
        return toHex( Base64.getDecoder().decode( base64String ) );
    }

    public static void gzipCompress( Path sourcePath, Path targetPath ) throws IOException {
        try( FileInputStream is = new FileInputStream( sourcePath.toFile() );
             GZIPOutputStream os = new GZIPOutputStream( new FileOutputStream( targetPath.toFile() ) ) ) {

            byte[] buffer = new byte[ 8096 ];
            int length;
            while( ( length = is.read( buffer ) ) > 0 ) {
                os.write( buffer, 0, length );
            }
        }
    }

    public static class StringResource extends ByteArrayResource {
        private final String fileName;
        public final String text;

        public StringResource( String text, String fileName ) {
            super( text.getBytes( StandardCharsets.UTF_8 ) );
            this.fileName = fileName;
            this.text = text;
        }

        public StringResource( byte[] data, String fileName ) {
            super( data );
            this.fileName = fileName;
            this.text = "";
        }

        @Override
        public String getFilename() {
            return fileName;
        }
    }
}

