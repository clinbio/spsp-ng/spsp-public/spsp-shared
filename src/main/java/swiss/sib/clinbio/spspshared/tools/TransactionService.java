package swiss.sib.clinbio.spspshared.tools;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import swiss.sib.clinbio.spspshared.tools.Tools;

@Service
@Transactional( propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class )
public class TransactionService {

    public <E extends Exception> void runInNewTransaction( Tools.Runnable<E> runnable ) throws E {
        runnable.call();
    }

    public static boolean isTransactionActive() {
        return TransactionSynchronizationManager.isActualTransactionActive();
    }

    public static boolean isNewTransaction() {
        return isTransactionActive() && TransactionAspectSupport.currentTransactionStatus().isNewTransaction();
    }

    public static boolean isCurrentTransactionReadOnly() {
        return TransactionSynchronizationManager.isCurrentTransactionReadOnly();
    }

/*
    @Autowired
    PlatformTransactionManager transactionManager;
    new TransactionTemplate(transactionManager).execute( (TransactionStatus status) -> {
    });
*/
}
