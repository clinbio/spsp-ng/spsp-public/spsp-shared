package swiss.sib.clinbio.spspshared.tools;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import swiss.sib.clinbio.spspshared.task.TaskScheduler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.StringJoiner;

@RestController
@RequestMapping
@Slf4j
public class SharedHomeController {
    @Autowired
    TaskScheduler taskScheduler;


    @GetMapping( "/isRunning" )
    public ResponseEntity<String> isRunning() {
        return ResponseEntity.ok( "ok " + SharedProperties.get().getApplicationName() );
    }

    @GetMapping( "/isRunningTask" )
    public ResponseEntity<String> isRunningTask() {
        return ResponseEntity.ok( taskScheduler.isEnabled() ? "ok" : "stopped" + " task " + SharedProperties.get().getApplicationName() );
    }

    @GetMapping( "/start_task_scheduler" )
    public ResponseEntity<?> startTaskScheduler( HttpServletRequest request, HttpServletResponse response ) {
        taskScheduler.start();
        return redirectToReferer( request, response );
    }

    @GetMapping( "/end_task_scheduler" )
    public ResponseEntity<?> endTaskScheduler( HttpServletRequest request, HttpServletResponse response ) {
        taskScheduler.stop();
        return redirectToReferer( request, response );
    }

    @GetMapping( "/log_sql" )
    public void logSql( @RequestParam( defaultValue = "DEBUG" ) String level ) {
        setLoggingLevel( "org.hibernate.SQL", level );
    }

    @GetMapping( "/logging_set" )
    public void loggingSet( @RequestParam String name, @RequestParam String level ) {
        setLoggingLevel( name, level );
    }

    public static void setLoggingLevel( String name, String level ) {
        Level logLevel = Level.toLevel( level );
        LoggerContext loggerContext = ( LoggerContext ) LoggerFactory.getILoggerFactory();
        loggerContext.getLogger( name ).setLevel( Level.toLevel( level ) );
        log.info( "Set logging '" + name + "' " + logLevel.levelStr );
    }

    public static ResponseEntity<String> displayLog( Path logFile ) throws IOException {
        List<String> rows = Files.readAllLines( logFile );
        StringJoiner joiner = new StringJoiner( "\n" );
        for( int i = rows.size() - 1; i >= 0; --i )
            joiner.add( rows.get( i ) );
        return ResponseEntity.ok( Tools.htmlText( joiner.toString() ) );
    }

    public static ResponseEntity<?> redirectToReferer( HttpServletRequest request, HttpServletResponse response ) {
        try {
            String referer = request.getHeader( "Referer" );
            if( referer != null )
                response.sendRedirect( referer );
            else
                response.sendRedirect( "/" );
        } catch( IOException ex ) {
        }
        return ResponseEntity.status( 302 ).build();
    }
}
