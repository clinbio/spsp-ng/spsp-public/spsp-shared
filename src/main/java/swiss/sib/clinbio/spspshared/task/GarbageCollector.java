package swiss.sib.clinbio.spspshared.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class GarbageCollector {
    static long lastUsedMemory = 50 << 20;

    public static void runWhenNeeded() {
        if( getUsedMemory() > 2 * lastUsedMemory )
            run();
    }

    public static void run() {
        long usedMemory = getUsedMemory();
        Runtime.getRuntime().gc();
        lastUsedMemory = getUsedMemory();
        log.info( "Memory: " + toMB( lastUsedMemory ) + " MB ( -" + toMB( usedMemory - lastUsedMemory ) + " MB )" );
    }

    public static long getUsedMemory() {
        //return ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed();
        return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    }

    public static String getMemoryUsage() {
        long jvmMemory = Runtime.getRuntime().totalMemory();
        long usedMemory = jvmMemory - Runtime.getRuntime().freeMemory();
        return "Memory used: " + toMB( usedMemory ) + " MB (jvm: " + toMB( jvmMemory ) + " MB)";
    }

    public static long toMB( long byteCount ) {
        return byteCount >> 20;
    }
}
