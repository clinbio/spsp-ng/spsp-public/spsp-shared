package swiss.sib.clinbio.spspshared.task;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

@Slf4j
public class SingletonRunner {
    private final AtomicBoolean isRunning = new AtomicBoolean( false );
    private String runningTaskName;

    public boolean requestRun( String taskName ) {
        if( !isRunning.compareAndSet( false, true ) ) {
            log.info( taskName + " already running" );
            return false;
        }

        log.info( "RUN " + taskName );
        runningTaskName = taskName;
        return true;
    }

    public synchronized void notifyFinish() {
        isRunning.set( false );
        log.info( "END " + runningTaskName );
        notifyAll();
    }

    public boolean isRunning() {
        return isRunning.get();
    }

    public synchronized void waitFinish() {
        while( isRunning() ) {
            try {
                wait();
            } catch( InterruptedException ex ) {
            }
        }
    }

    public static class Process {
        public boolean endRequested = false;
        public final Progress progress = new Progress();
        public final Supplier<String> progressInfo = null;

        public String getProgressInfo() {
            if( progressInfo != null )
                return progressInfo.get();
            return progress.getInfo();
        }

        public static class Progress {
            public String name;
            public int current = -1;
            public int max = -1;

            public void init( String name, int max ) {
                this.name = name;
                this.max = max;
                current = 0;
            }

            public void incr() {
                ++current;
            }

            public void set( String name ) {
                this.name = name;
                current = max = -1;
            }

            public String getInfo() {
                if( max > 0 )
                    return name + ": " + current + "/" + max;
                if( current > 0 )
                    return name + ": " + current;
                return name;
            }
        }
    }
}
