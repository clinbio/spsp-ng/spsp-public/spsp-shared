package swiss.sib.clinbio.spspshared.task;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import swiss.sib.clinbio.spspshared.tools.Tools;

import java.io.IOException;
import java.nio.file.Path;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

@Service
@Slf4j
public class TaskScheduler {
    private boolean isEnabled = false;
    private Path taskPath;
    private Path logPath;
    final List<ScheduledTask> taskList = new ArrayList<>();
    final List<LogTask> taskLog = new ArrayList<>();
    private final Map<String, ScheduledTask> taskMap = new HashMap<>();
    private final Map<ScheduledTask, PeriodicSchedule> periodicTask = new HashMap<>();
    private final SingletonRunner.Process processInfo = new SingletonRunner.Process();
    private final ScheduledExecutorService timerScheduler = Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture<?> scheduledTask = null;
    List<ScheduledTask> nextRunList = null;
    private ScheduledTask runningTask = null;

    TaskScheduler() {
        scheduler = this;
    }

    public void run( Path propertiesFolder ) {
        taskPath = propertiesFolder.resolve( "TaskSchedule.json" );
        logPath = propertiesFolder.resolve( "TaskLog.json" );
        load();
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void start() {
        isEnabled = true;
        log.warn( "Task scheduler: " + isEnabled );
        schedule();
    }

    public void stop() {
        isEnabled = false;
        log.warn( "Task scheduler: stopped" );
        if( scheduledTask != null ) {
            scheduledTask.cancel( false );
            if( runningTask != null )
                runningTask.runner.requestEnd();
        }

        for( ScheduledTask task : taskList )
            if( task.isRunningInThread )
                task.runner.requestEnd();

        scheduledTask = timerScheduler.scheduleWithFixedDelay(
                () -> log.error( "Task scheduler is stopped" ), 60, 60, TimeUnit.MINUTES );
    }

    public void addTask( String name, TaskRun runner, PeriodicSchedule schedule ) {
        ScheduledTask task = addTask( name, runner );
        periodicTask.put( task, schedule );
    }

    public void runTask( String name, boolean withScheduler ) {
        ScheduledTask task = getTask( name );
        if( task != null && task.isActive() ) {
            task.setStartTime( LocalDateTime.now() );
            if( withScheduler || isEnabled ) {
                schedule();
            } else
                task.doRun( processInfo );
        } else
            log.error( "Task " + name + " not found" );
    }

    public void runTaskBefore( String name, int delayInSeconds ) {
        ScheduledTask task = getTask( name );
        if( task.isActive() ) {
            task.setStartTime( LocalDateTime.now().plus( delayInSeconds, ChronoUnit.SECONDS ) );
            schedule();
        }
    }

    public void runTaskAfter( String name, int delayInSeconds ) {
        ScheduledTask task = getTask( name );
        if( task.isActive() ) {
            task.setStartTimeAfter( LocalDateTime.now().plus( delayInSeconds, ChronoUnit.SECONDS ) );
            schedule();
        }
    }

    public void runTask( String name, int hour, int minute ) {
        LocalDateTime date = LocalDateTime.now();
        if( date.getHour() > hour || ( date.getHour() == hour && date.getMinute() >= minute ) )
            date = date.plus( 1, ChronoUnit.DAYS );
        date = LocalDateTime.of( date.getYear(), date.getMonth(), date.getDayOfMonth(), hour, minute );
        runTaskBefore( name, ( int ) LocalDateTime.now().until( date, ChronoUnit.SECONDS ) );
    }

    public ScheduledTask addTask( String name, TaskRun runner ) {
        ScheduledTask task = createTask( name );
        if( task.runner == null )
            taskList.add( task );

        task.runner = runner;
        return task;
    }

    public boolean isSoonRunning( String name, int spanInSeconds ) {
        ScheduledTask task = getTask( name );
        if( task == null )
            return false;
        if( task.isRunning() )
            return true;
        return LocalDateTime.now().until( task.nextStartTime, ChronoUnit.SECONDS ) <= spanInSeconds;
    }

    public ScheduledTask getTask( String name ) {
        ScheduledTask task = taskMap.get( name );
        if( task != null )
            return task;
        log.error( "Task unknown: " + name );
        return null;
    }

    private ScheduledTask createTask( String name ) {
        ScheduledTask task = taskMap.get( name );
        if( task != null )
            return task;
        task = new ScheduledTask( name );
        taskMap.put( task.name, task );
        return task;
    }

    private synchronized void schedule() {
        if( !isEnabled || ( runningTask != null ) )
            return;
        LocalDateTime now = LocalDateTime.now();
        for( Map.Entry<ScheduledTask, PeriodicSchedule> entry : periodicTask.entrySet() )
            if( entry.getKey().isActive() )
                entry.getValue().computeNextRunningTime( entry.getKey(), now );

        write();
        if( scheduledTask != null ) {
            scheduledTask.cancel( false );
            scheduledTask = null;
        }

        LocalDateTime firstTime = LocalDateTime.MAX;
        ScheduledTask firstTask = null;
        nextRunList = new ArrayList<>();
        for( ScheduledTask task : taskList ) {
            LocalDateTime time = task.nextStartTime;
            if( time == null || !task.isActive() || task.isRunningInThread )
                continue;
            if( time.isBefore( now ) )
                nextRunList.add( task );
            if( firstTime.isAfter( time ) ) {
                firstTime = time;
                firstTask = task;
            }
        }
        if( firstTask == null )
            return;

        if( nextRunList.isEmpty() )
            nextRunList.add( firstTask );
        else
            nextRunList.sort( Comparator.comparing( o -> o.nextStartTime ) );

        long waitingTime = now.until( firstTime, ChronoUnit.MILLIS );
        if( waitingTime > 0 )
            scheduledTask = timerScheduler.schedule( this::runNextTask, waitingTime, TimeUnit.MILLISECONDS );
        else
            runNextTask();
    }

    private void runNextTask() {
        System.out.println( "------------------------" + Tools.timeToString( new Date() ) );
        for( ScheduledTask task : nextRunList ) {
            if( !isEnabled() || processInfo.endRequested )
                break;
            runningTask = task;
            task.doRun( processInfo );
        }
        runningTask = null;
        nextRunList = null;
        schedule();
    }

    private void load() {
        try {
            ScheduledTask[] tasks = Tools.jsonMapper().readValue( taskPath.toFile(), ScheduledTask[].class );
            for( ScheduledTask task : tasks ) {
                if( task.nextStartTime.until( ScheduledTask.WAS_INTERRUPTED, ChronoUnit.SECONDS ) == 0 )
                    task.nextStartTime = LocalDateTime.now();
                taskMap.put( task.name, task );
            }
            if( taskLog.isEmpty() ) {
                LogTask[] logs = Tools.jsonMapper().readValue( logPath.toFile(), LogTask[].class );
                LocalDateTime sinceDate = LocalDateTime.of( LocalDate.now().minusDays( 8 ), LocalTime.of( 0, 0 ) );
                for( LogTask log : logs )
                    if( log.time.isAfter( sinceDate ) )
                        taskLog.add( log );
            }
        } catch( IOException ex ) {
            log.error( ex.toString() );
        }
    }

    private void write() {
        Tools.writeJson( taskList, taskPath );
        Tools.writeJson( taskLog, logPath );
    }

    public static String runTaskInThread( TaskScheduler.ScheduledTask task, Supplier<String> runnable ) {
        if( task.isRunningInThread ) {
            log.error( "ASSERT runTaskInThread" );
            return "";
        }
        task.isRunningInThread = true;
        new Thread( () -> {
            try {
                String status = runnable.get();
                task.log.set( status );

            } catch( Exception ex ) {
                log.error( task.name + ": " + ex );
                task.log.set( "fail" );
            } finally {
                task.notifyRunningEnd();
                TaskScheduler.get().schedule();
            }
        }
        ).start();
        return "ongoing";
    }

    private static TaskScheduler scheduler;

    public static TaskScheduler get() {
        return scheduler;
    }

    public interface TaskRun {
        String run( ScheduledTask task, SingletonRunner.Process processInfo );

        void requestEnd();
    }

    public static class ScheduledTask {
        public final String name;
        public String data;
        public LocalDateTime nextStartTime = NOT_SCHEDULED;
        public LocalDateTime lastStartTime = LocalDateTime.of( 2000, 1, 1, 0, 0, 0 );
        public Integer lastRunningDurationInSeconds = -1;
        @JsonIgnore
        private TaskRun runner;
        @JsonIgnore
        public boolean isRunning = false;
        @JsonIgnore
        public boolean isRunningInThread = false;
        @JsonIgnore
        public LogTask log;

        ScheduledTask( String name ) {
            this.name = name;
        }

        private ScheduledTask() {
            name = "";
        } //for JSON

        boolean isActive() {
            return runner != null;
        }

        boolean isRunning() {
            return isRunning || isRunningInThread;
        }

        private static final LocalDateTime NOT_SCHEDULED = LocalDateTime.of( 2222, 2, 22, 22, 22, 22 );
        private static final LocalDateTime WAS_INTERRUPTED = LocalDateTime.of( 2111, 11, 11, 11, 11, 11 );

        void doRun( SingletonRunner.Process processInfo ) {
            if( runner == null )
                return;

            try {
                isRunning = true;
                lastStartTime = LocalDateTime.now();
                nextStartTime = WAS_INTERRUPTED;
                log = new LogTask( name, lastStartTime );
                processInfo.progress.set( name );
                String message = runner.run( this, processInfo );
                log.set( message );
                if( !message.isBlank() )
                    TaskScheduler.get().taskLog.add( log );

            } catch( Exception ex ) {
                TaskScheduler.log.error( name + ": " + ex );
                log.set( "fail" );
            } finally {
                if( !isRunningInThread )
                    notifyRunningEnd();
            }
        }

        void notifyRunningEnd() {
            isRunning = isRunningInThread = false;
            lastRunningDurationInSeconds = log.runningDurationInSeconds;
            if( nextStartTime.until( WAS_INTERRUPTED, ChronoUnit.SECONDS ) == 0 )
                nextStartTime = NOT_SCHEDULED;
            GarbageCollector.runWhenNeeded();
        }

        public void setStartTime( LocalDateTime nextRunTime ) {
            if( nextStartTime.isAfter( nextRunTime ) )
                setStartTimeAfter( nextRunTime );
        }

        public void setStartTimeAfter( LocalDateTime nextRunTime ) {
            if( lastStartTime.until( nextRunTime, ChronoUnit.MILLIS ) < 1000 )
                nextStartTime = lastStartTime.plus( 1000, ChronoUnit.MILLIS );
            else
                nextStartTime = nextRunTime;
        }
    }

    public static class LogTask {
        public final String name;
        public final LocalDateTime time;
        public Integer runningDurationInSeconds;
        public String message;

        LogTask( String name, LocalDateTime time ) {
            this.name = name;
            this.time = time;
        }

        public synchronized void set( String message ) {
            runningDurationInSeconds = ( int ) time.until( LocalDateTime.now(), ChronoUnit.SECONDS );
            this.message = message.strip();
        }

        private LogTask() {
            name = "";
            time = LocalDate.of( 2000, 1, 1 ).atStartOfDay();
        } //for JSON
    }

    static final int DAY_IN_SECONDS = 24 * 3600;
    static final int WEEK_IN_SECONDS = 7 * DAY_IN_SECONDS;

    public static int getDayTime( int hour, int minute ) {
        return hour * 3600 + minute * 60;
    }

    public static int getWeekTime( DayOfWeek day, int hour, int minute ) {
        return ( day.getValue() - 1 ) * DAY_IN_SECONDS + getDayTime( hour, minute );
    }

    public static int getWeekTime( LocalDateTime time ) {
        return getWeekTime( time.getDayOfWeek(), time.getHour(), time.getMinute() ) + time.getSecond();
    }

    public static int getMonthTime( int day, int hour, int minute ) {
        return ( day - 1 ) * DAY_IN_SECONDS + getDayTime( hour, minute );
    }

    public static int getMonthTime( LocalDateTime time ) {
        return getMonthTime( time.getDayOfMonth(), time.getHour(), time.getMinute() ) + time.getSecond();
    }

    static abstract class PeriodicSchedule {
        private final int[] timeList;

        protected PeriodicSchedule( int maxSpanInSeconds, int... timeInSeconds ) {
            int prev = 0;
            for( int v : timeInSeconds ) {
                if( v < prev )
                    throw new RuntimeException( "PeriodicSchedule need increasing values" );
                if( v >= maxSpanInSeconds )
                    throw new RuntimeException( "PeriodicSchedule has not the right unit" );
                prev = v;
            }
            this.timeList = timeInSeconds;
        }

        private void computeNextRunningTime( ScheduledTask task, LocalDateTime now ) {
            LocalDateTime nextTime = now.plus( getTimeSpan( now ), ChronoUnit.SECONDS );
            task.setStartTime( nextTime );
        }

        protected abstract int getTimeSpan( LocalDateTime time );

        protected int getTimeSpan( int timeInSeconds, int maxSpanInSeconds ) {
            for( int j : timeList ) {
                if( timeInSeconds < j )
                    return j - timeInSeconds;
            }

            return maxSpanInSeconds - timeInSeconds + timeList[ 0 ];
        }
    }

    public static class MonthlySchedule extends PeriodicSchedule {
        public MonthlySchedule( int... timeInSeconds ) {
            super( 31 * DAY_IN_SECONDS, timeInSeconds );
        }

        @Override
        protected int getTimeSpan( LocalDateTime time ) {
            return getTimeSpan( getMonthTime( time ), YearMonth.now().lengthOfMonth() * DAY_IN_SECONDS );
        }
    }

    public static class WeeklySchedule extends PeriodicSchedule {
        public WeeklySchedule( int... timeInSeconds ) {
            super( WEEK_IN_SECONDS, timeInSeconds );
        }

        @Override
        protected int getTimeSpan( LocalDateTime time ) {
            return getTimeSpan( getWeekTime( time ), WEEK_IN_SECONDS );
        }
    }

    public static class DailySchedule extends PeriodicSchedule {
        public DailySchedule( int... timeInSeconds ) {
            super( DAY_IN_SECONDS, timeInSeconds );
        }

        @Override
        protected int getTimeSpan( LocalDateTime time ) {
            return getTimeSpan( time.toLocalTime().toSecondOfDay(), DAY_IN_SECONDS );
        }
    }

    public static class HourSchedule extends PeriodicSchedule {
        public HourSchedule( double... timeInMinutes ) {
            super( 3600, convertToSeconds( timeInMinutes ) );
        }

        @Override
        protected int getTimeSpan( LocalDateTime time ) {
            return getTimeSpan( time.toLocalTime().getMinute() * 60 + time.toLocalTime().getSecond(), 3600 );
        }

        static int[] convertToSeconds( double... timeInMinutes ) {
            int[] timeList = new int[ timeInMinutes.length ];
            for( int i = 0; i < timeInMinutes.length; i++ )
                timeList[ i ] = ( int ) Math.round( timeInMinutes[ i ] * 60 );
            return timeList;
        }
    }

    public static class MinuteSchedule extends PeriodicSchedule {
        public MinuteSchedule( int... timeInSeconds ) {
            super( 60, timeInSeconds );
        }

        @Override
        protected int getTimeSpan( LocalDateTime time ) {
            return getTimeSpan( time.toLocalTime().getSecond(), 60 );
        }
    }
}