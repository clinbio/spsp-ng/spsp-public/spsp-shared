package swiss.sib.clinbio.spspshared.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import swiss.sib.clinbio.spspshared.tools.Tools;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class TaskReport {
    @Autowired
    TaskScheduler scheduler;

    public Report getReport() {
        Report report = new Report();
        List<TaskScheduler.ScheduledTask> taskList = new ArrayList<>( scheduler.taskList );
        taskList.sort( Comparator.comparing( t -> t.nextStartTime ) );

        for( TaskScheduler.ScheduledTask task : taskList ) {
            Report.Task.Status status = Report.Task.Status.NONE;
            if( task.isRunning() ) {
                status = Report.Task.Status.RUNNING;
                report.isRunning = true;
            } else if( scheduler.nextRunList != null && scheduler.nextRunList.contains( task ) ) {
                status = Report.Task.Status.WAITING;
            }

            Report.Task reportTask = new Report.Task( task, status );
            if( status == Report.Task.Status.RUNNING )
                report.taskList.add( 0, reportTask );
            else
                report.taskList.add( reportTask );
        }

        for( int i = scheduler.taskLog.size() - 1; i >= 0; --i ) {
            report.logList.add( new Report.Log( scheduler.taskLog.get( i ) ) );
        }
        return report;
    }

    public static class Report {
        public final List<Task> taskList = new ArrayList<>();
        public final List<Log> logList = new ArrayList<>();
        public boolean isRunning = false;

        static class Task {
            public final String name;
            public final String nextRunning;
            public final String lastRunning;
            public final int lastDuration;

            enum Status {NONE, RUNNING, WAITING}

            public final Status status;

            Task( TaskScheduler.ScheduledTask task, Status status ) {
                name = task.name;
                nextRunning = task.nextStartTime != null ? Tools.timeToString( task.nextStartTime ) : "-";
                lastRunning = task.lastStartTime != null ? Tools.timeToString( task.lastStartTime ) : "-";
                lastDuration = task.lastRunningDurationInSeconds;
                this.status = status;
            }

            public boolean isRunning() {
                return status == Status.RUNNING;
            }

            public boolean isWaiting() {
                return status == Status.WAITING;
            }
        }

        static class Log {
            public final String name;
            public final String time;
            public final int duration;
            public final String message;

            Log( TaskScheduler.LogTask log ) {
                name = log.name;
                time = Tools.timeToString( log.time );
                duration = log.runningDurationInSeconds;
                message = log.message;
            }
        }
    }
}
