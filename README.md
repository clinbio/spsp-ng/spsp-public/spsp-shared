# SPSP Common Libraries

## Description
The SPSP Common Libraries contains code that is used by several services of SPSP and is therefore shared across them.

## Authors
Main developer: Daniel Walther

## License
The source code is licensed under GPL-3.0-or-later and available via GitLab.
For more information or any inquiries, please reach out to legal@sib.swiss.